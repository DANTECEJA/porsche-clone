let currentSlide = 0;
const totalSlides = document.querySelectorAll('#carousel img').length;

function moveCarousel(direction) {
    currentSlide += direction;
    if (currentSlide < 0) {
        currentSlide = totalSlides - 1;
    } else if (currentSlide >= totalSlides) {
        currentSlide = 0;
    }
    document.getElementById('carousel').style.transform = `translateX(-${currentSlide * 25}%)`;
}
