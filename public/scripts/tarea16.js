tailwind.config = {
    theme: {
        extend: {
            keyframes: {
                expandWidth: {
                    '0%': { width: '0%' },
                    '100%': { width: '100%' },
                }
            },
            animation: {
                expandWidth: 'expandWidth 0.5s ease-in-out forwards'
            }
        }
    }
}
function toggleParagraph(element) {
    // Obtener todos los elementos p dentro del contenedor
    const paragraphs = element.parentElement.querySelectorAll('p');
    // Ocultar todos los elementos p
    paragraphs.forEach(paragraph => paragraph.classList.add('hidden'));
    // Mostrar el elemento p correspondiente al h2 clicado
    element.nextElementSibling.classList.remove('hidden');
}

function changeBackground(imageUrl) {
    document.getElementById('car-section').style.backgroundImage = `url('${imageUrl}')`;
}

function toggleLine(element) {
    // Obtener todos los elementos span dentro del contenedor
    const lines = element.parentElement.querySelectorAll('span');
    // Reiniciar todas las líneas a su estado inicial
    lines.forEach(line => line.classList.remove('w-full'));
    // Expandir la línea asociada al h2 clicado
    element.querySelector('span').classList.add('w-full');
}