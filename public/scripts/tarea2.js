play_pause= document.getElementById('play')
control_play = false
video = document.getElementById('video')

function play(){
    if(control_play){
        control_play=false
        play_pause.setAttribute('name', 'pause-outline')
        video.pause()
    }else{
        control_play=true
        play_pause.setAttribute('name', 'play-outline')
        video.play()
    }
}